---
layout: post
title: Welcome!
---

I am using Jekyll to create this blog from [Jekyll Now repository](https://github.com/barryclark/jekyll-now) on GitHub. I am posting about new technologies I learn as I commit to continuos learning.

Interested in my [my acrylic pour art](https://github.com/emilydaitch/emilydaitch.github.io/acrylic) ?